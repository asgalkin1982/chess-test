<?php

require_once __DIR__ . '/vendor/autoload.php';

try {
    $board = new Board();

    $args = $argv;
    array_shift($args);

    foreach ($args as $move) {
        $motion = $board->getMotion($move);
        if ($board->checkMotion($motion)) {
            $board->move($move);
            $board->addMotion($motion);
        }
    }

    $board->dump();
} catch (\Exception $e) {
    echo $e->getMessage() . "\n";
    exit(1);
}
