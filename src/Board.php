<?php

class Board {
    private $figures = [];

    private $motions = [];

    public function __construct() {
        $this->figures['a'][1] = new Rook(false);
        $this->figures['b'][1] = new Knight(false);
        $this->figures['c'][1] = new Bishop(false);
        $this->figures['d'][1] = new Queen(false);
        $this->figures['e'][1] = new King(false);
        $this->figures['f'][1] = new Bishop(false);
        $this->figures['g'][1] = new Knight(false);
        $this->figures['h'][1] = new Rook(false);

        $this->figures['a'][2] = new Pawn(false);
        $this->figures['b'][2] = new Pawn(false);
        $this->figures['c'][2] = new Pawn(false);
        $this->figures['d'][2] = new Pawn(false);
        $this->figures['e'][2] = new Pawn(false);
        $this->figures['f'][2] = new Pawn(false);
        $this->figures['g'][2] = new Pawn(false);
        $this->figures['h'][2] = new Pawn(false);

        $this->figures['a'][7] = new Pawn(true);
        $this->figures['b'][7] = new Pawn(true);
        $this->figures['c'][7] = new Pawn(true);
        $this->figures['d'][7] = new Pawn(true);
        $this->figures['e'][7] = new Pawn(true);
        $this->figures['f'][7] = new Pawn(true);
        $this->figures['g'][7] = new Pawn(true);
        $this->figures['h'][7] = new Pawn(true);

        $this->figures['a'][8] = new Rook(true);
        $this->figures['b'][8] = new Knight(true);
        $this->figures['c'][8] = new Bishop(true);
        $this->figures['d'][8] = new Queen(true);
        $this->figures['e'][8] = new King(true);
        $this->figures['f'][8] = new Bishop(true);
        $this->figures['g'][8] = new Knight(true);
        $this->figures['h'][8] = new Rook(true);
    }

    public function move($move) {
        if (!preg_match('/^([a-h])(\d)-([a-h])(\d)$/', $move, $match)) {
            throw new \Exception("Incorrect move");
        }

        $xFrom = $match[1];
        $yFrom = $match[2];
        $xTo   = $match[3];
        $yTo   = $match[4];

        if (isset($this->figures[$xFrom][$yFrom])) {
            $this->figures[$xTo][$yTo] = $this->figures[$xFrom][$yFrom];
        }
        unset($this->figures[$xFrom][$yFrom]);
    }

    public function dump() {
        for ($y = 8; $y >= 1; $y--) {
            echo "$y ";
            for ($x = 'a'; $x <= 'h'; $x++) {
                if (isset($this->figures[$x][$y])) {
                    echo $this->figures[$x][$y];
                } else {
                    echo '-';
                }
            }
            echo "\n";
        }
        echo "  abcdefgh\n";
    }

    public function addMotion($motion): void
    {
        $this->motions[] = $motion;
    }

    public function getPreviousMotion(): ?Motion
    {
        $key = array_key_last($this->motions);
        return $key ? $this->motions[$key] : null;
    }

    public function checkMotion(Motion $motion): bool
    {
        return $this->checkSequence($motion)
            && $this->checkPawnMove($motion)
            ;
    }

    private function checkPawnMove(Motion $motion): bool
    {
        $isBlack = $motion->figure->isBlack();
        $dY = $motion->yTo - $motion->yFrom;
        $dx = ord($motion->xTo) - ord($motion->xFrom);
        $isStartPosition = $isBlack
            ? (7 == $motion->yFrom)
            : (2 == $motion->yFrom);

        switch(true) {

            case ($isStartPosition && abs($dY) > 2):
            case (!$isStartPosition && abs($dY) > 1):

            case (!$isBlack && $dY < 1):
            case ($isBlack && $dY > -1):

            case ((abs($dY) === 2) && isset($this->figures[$motion->xFrom][($motion->yFrom + $motion->yTo)/2])):
            case ($dx === 0 && isset($this->figures[$motion->xFrom][$motion->yTo])):

            case (abs($dx) === 1 && !isset($this->figures[$motion->xTo][$motion->yTo])):

                throw new  Exception('Недопустимый ход пешки. ' . print_r($motion, true));
        }

        return true;
    }

    private function checkSequence(Motion $motion): bool
    {
        $previousMotion = $this->getPreviousMotion();

        if ($previousMotion && $motion->figure->isBlack() === $previousMotion->figure->isBlack()
        ) {
            throw new  Exception('Игроки должны ходить поочередно.');
        }

        return true;
    }

    public function getMotion(string $move): Motion
    {
        if (!preg_match('/^([a-h])(\d)-([a-h])(\d)$/', $move, $match)) {
            throw new \Exception("Incorrect move");
        }

        $xFrom = $match[1];
        $yFrom = $match[2];
        $xTo   = $match[3];
        $yTo   = $match[4];

        $figure = $this->figures[$xFrom][$yFrom];
        return new Motion($figure, $xFrom, $yFrom, $xTo, $yTo);
    }

}
