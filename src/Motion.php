<?php
declare(strict_types=1);

class Motion
{
    public Figure $figure;
    public string $xFrom;
    public string $yFrom;
    public string $xTo;
    public string $yTo;
    public $isBlack;

    /**
     * @param Figure $figure
     * @param string $xFrom
     * @param string $yFrom
     * @param string $xTo
     * @param string $yTo
     */
    public function __construct(Figure $figure, string $xFrom, string $yFrom, string $xTo, string $yTo)
    {
        $this->figure = $figure;
        $this->xFrom = $xFrom;
        $this->yFrom = $yFrom;
        $this->xTo = $xTo;
        $this->yTo = $yTo;
    }


}